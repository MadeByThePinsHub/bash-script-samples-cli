[![Gitpod Ready-to-Code](https://img.shields.io/badge/Gitpod-Ready--to--Code-blue?logo=gitpod)](https://gitpod.io/#https://gitlab.com/MadeByThePinsTeam-DevLabs/bash-script-samples-cli) 

# Bash Script Samples CLI

**WARNING: The CLI is in alpha phrase, USE THIS at your OWN RISK!**

## Docs
Call the `docs` command to get up-to-date link or visit <https://bash-samples-cli-docs.herokuapp.com> to read the docs.

## Installation
```bash
## This script will automates the cloning of this Git repo, updating the PATH and othe stuff.
curl -sL https://bash-script-samples-api.glitch.me/installAPIWrapper | bash -

## After installation, try to issue the help text.
BashScriptSamplesCLI help
```

## Updating CLI
Before you update the CLI, update Git first and check your internet.
```bash
## Issue the update command
BasScriptSamplesCLI update-cli
## [INFO] Checking version if's upgradable...
## [INFO] Your current version is 0.1.0-alpha and you need to update the CLI immediately.
## [INFO] Connecting to the API endpoint...
## [INFO] API is online: 200 OK
## [INFO] Getting update status from API endpoint...
## ...
## [INFO] Running post-update stuff...
## [SUCCESS] Update completed! Try 'BashScriptSamplesCLI help` now.
## [INFO] Clearing up...

## Verify if it's up.
BashScriptSamplesCLI version
## Current version: 0.1.0-alpha
## Branch: master
## Status: Under development
## Source: Git Repository Clone / Branch Tarball Snapshots
## Git repository URL: https://gitlab.com/MadeByThePinsTeam-DevLabs/bash-script-samples-cli
```

## Disabling and Enabling
```bash
## This will disables the BashScriptSamplesCLI commands as this removes from your PATH.
BashScriptSamplesAPI disable-cli

## Miss the CLI? Enable it as this add to your PATH.
$CLI_INSTALLATION_DIR/BashScriptSamplesCLI.sh enable-cli
```

## Uninstalling
We are sorry to see you go. You'll be prompted twice before you uninstall if you use `BashScriptSamplesCLI uninstall`.
```bash
## Trigger the uninstall process forcibly
BashScriptSamplesCLI uninstall --force

## Or, remove the directory and reset your PATH
rm -rf $CLI_INSTALLATION_DIR
```
