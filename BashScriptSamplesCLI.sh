#!/bin/bash

## Script Metadata
## If you're not want to edit this file, PLEASE QUIT YOUR EDITOR IMMEDIATELY!
CLI_VERSION=0.1.0-alpha
CLI_API_ENDPOINT_HOST=https://bash-script-samples-api.glitch.me
CLI_USE_GRAPHIQL=false
CLI_INSTALLATION_PATH=$CLI_INSTALLATION_PATH
CLI_USER_CONFIG=$HOME/config.yml

## User configuration
CLI_LANG=$CLI_LANG

#Set name
NAME=$(basename $0)

# Help
usage="
Bash Script Samples CLI v$CLI_VERSION by the Pins team

Hello. The Bash Script Samples was built by the Pins team to
automate your tasks without manually typing commands.

Usage:  ${NAME} [OPTIONS] [ARGS]

Available CLI options are:
  help                  Show this help message.
  docs                  Get the link to the online documentation.
  git-clone             Clone the full Git repository from our GitLab templates repo.
  update-cli            Update the CLI code. This will download and run the script updater.
  version               Prints th version of the CLI.
  config                Configuration stuff
  global-install        Run the global installation, but requires permission to use 'sudo'.
  run <template-name>   Run the template from your 'CLI_INSTALLATION_DIR/templates'.
  enable-cli            Enable CLI access by adding the installation dir to your PATH.
  disable-cli           Disable easy CLI access by resetting your PATH to defaults.
  setup                 Use this command if you're installed me from Git source.
  dev                   Enable dev mode for developers.
  
Available global CLI arguments are:
  -h, --help   Show help for this option, otherwise fall backs to this message.
  --version,
    -v         Same as the \"version\" command.
"

cli_help() {
	echo "${usage}"
	exit 0
}

exportPathBeforeEnablingCLI() {
	bash $CLI_INSTALLATION_DIR/source/commands/enable-cli.sh
}

cloneScriptCollection() {
	echo "[INFO] Preparing your installation directory, buckle up!"
	echo "[INFO] To abort, press Ctrl+D"
}

getOnlineDocsLink() {
	echo "[INFO] Fetching data from the source files..."
	sleep 5
	cat $CLI_INSTALLATION_PATH/source/docs.txt
	exit 0
}

catchOnError() {
	echo "[ERR] The CLI doesn't understand that. Try reading 'help' again."
	exit 1
}

setupCli() {
    echo "[INFO] Warming up..."
}


if [[ ! -f $CLI_USER_CONFIG ]]; then
    echo "[WARN] No configuration file found in your home directory."
    echo "[INFO] Running command in 5 seconds..."
    sleep 5
elif [[ ! -f $CLI_INSTALLATION_DIR/config.yml ]]; then
    echo "[WARN] No configuration file found in your home directory."
    echo "[INFO] Running command in 5 seconds..."
    sleep 5
else
    echo ""
fi

## Commands Stuff
if [[ $1 == "" ]]; then
	echo "[INFO] Sent a blank command, so this is not a error. Try using 'help' instead."
    echo "[INFO] Exiting..."
    exit 0
elif [[ $1 == "version" ]]; then
  	cat $CLI_INSTALLATION_DIR/version.txt
elif [[ $1 == "docs" ]]; then
	getOnlineDocsLink
elif [[ $1 == "help" ]]; then
    	cli_help
elif [[ $1 == "-h" ]]; then
    	cli_help
elif [[ $1 == "--help" ]]; then
    	cli_help
elif [[ $1 == "git-clone" ]]; then
	cloneScriptCollection
elif [[ $1 == "setup" ]]; then
    setupCli
elif [[ $1 == "dev" ]]; then
    cat $CLI_INSTALLATION_DIR/source/help-text/dev.txt
else
    catchOnError
fi

